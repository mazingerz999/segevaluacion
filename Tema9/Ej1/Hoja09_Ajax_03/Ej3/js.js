$(document).ready(function () {

    //LABELS
        let errorNombre = $("#errorNombre");
        let errorDNI = $("#errorDNI");
        let errorPassword = $("#errorPassword");
    
        $("#enviar").click(function (e) {
            let validado = true;
            e.preventDefault();
    
            // $("#errores").empty();
    
            let nombre = $("#nombre").val();
            let dni = $("#dni").val();
            let password1 = $("#password1").val();
            let password2 = $("#password2").val();
    
            $.ajax({
                url: "validar.php",
    
                data: {
                    'nombre': nombre,
                    'dni': dni,
                    'password1': password1,
                    'password2': password2
                },
    
                type: 'POST',
    
                dataType: 'json',
    
                success: function (datos) {
    
                    
                    if (datos.errorNombre) {
                        
                        $("#errores").css("color", "red");
                        $("#errores").append(datos.errorNombre+"<br>");
                        validado = false;
                    }
                    if (datos.errorDNI) {
                        $("#errores").css("color", "red");
                        $("#errores").append(datos.errorDNI+"<br>");
                        validado = false;
                    }
                    if (datos.errorPassword) {
                        $("#errores").css("color", "red");
                        $("#errores").append(datos.errorPassword+"<br>");
                        validado = false;
                    }
                    if (validado) {
                        $("#form1").submit();
                     
                    }
                    
                },
    
                error: function (xhr, Status) {
                    alert("Disculpe existio un problema");
    
                }
    
            });
    
        });
    
    });