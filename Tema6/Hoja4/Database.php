<?php
require_once "Producto.php";
require_once "Alimentacion.php";
require_once "Electronica.php";
require_once "Categoria.php";


class Database
{
    private static $instance = null;
    const DSN = "mysql:host=localhost;dbname=dwes_supermercado";
    const USERNAME = "root";
    const PASSWORD = "";

    private function __construct()
    {
    }

    public static function getInstance(): Database
    {
        if (self::$instance === null) {
            self::$instance = new Database();
        }
        return self::$instance;
    }

    private function getConexion(): PDO
    {
        $opciones = array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8");
        return new PDO(
            self::DSN,
            self::USERNAME,
            self::PASSWORD,
            $opciones
        );
    }
    public function getElectronica(){
        $datos=null;
        $conexion = $this->getConexion();
        $sql = /** @lang MariaDB */
            "SELECT p.codigo, p.precio, p.nombre, e.plazo_garantia, c.id, c.nombre FROM productos as p inner join electronica as e on p.codigo=e.id_electronica inner join categorias as c on c.id=p.categoria";
        $consulta = $conexion->query($sql);
        $consulta->bindColumn(1, $id);
        $consulta->bindColumn(2, $precio);
        $consulta->bindColumn(3, $nombreProducto);
        $consulta->bindColumn(4, $idCategoria);
        $consulta->bindColumn(5, $plazo);
        $consulta->bindColumn(6, $nombreCategoria);
        while ($fila = $consulta->fetch(PDO::FETCH_BOUND)) {
            $datos[] = new Electronica($id, $precio, $nombreProducto, new Categoria($idCategoria, $nombreCategoria), $plazo);
        }
        return $datos;
    }
    public function getAlimentacion(){
        $datos=null;
        $conexion = $this->getConexion();
        $sql = /** @lang MariaDB */
            "SELECT p.codigo, p.precio, p.nombre, a.mes_caducidad,a.anio_caducidad, c.id, c.nombre FROM productos as p inner join alimentacion as a on p.codigo=a.id_alimentacion inner join categorias as c on c.id=p.categoria";
        $consulta = $conexion->query($sql);
        $consulta->bindColumn(1, $id);
        $consulta->bindColumn(2, $precio);
        $consulta->bindColumn(3, $nombreProducto);
        $consulta->bindColumn(4, $mescaducidad);
        $consulta->bindColumn(5, $anyocaducidad);
        $consulta->bindColumn(6, $idCategoria);
        $consulta->bindColumn(7, $nombreCategoria);
        while ($fila = $consulta->fetch(PDO::FETCH_BOUND)) {
            $datos[] = new Alimentacion($id, $precio, $nombreProducto, new Categoria($idCategoria, $nombreCategoria), $mescaducidad, $anyocaducidad);
        }
        return $datos;
    }
    public function getProductos(){
        $datos=null;
        $conexion = $this->getConexion();
        $sql = /** @lang MariaDB */
            "SELECT p.codigo, p.precio, p.nombre, c.id, c.nombre FROM productos as p  inner join categorias as c on c.id=p.categoria";
        $consulta = $conexion->query($sql);
        $consulta->bindColumn(1, $id);
        $consulta->bindColumn(2, $precio);
        $consulta->bindColumn(3, $nombreProducto);
        $consulta->bindColumn(4, $idCategoria);
        $consulta->bindColumn(5, $nombreCategoria);
        while ($fila = $consulta->fetch(PDO::FETCH_BOUND)) {
            $datos[] = new Producto($id, $precio, $nombreProducto, new Categoria($idCategoria, $nombreCategoria));
        }
        return $datos;
    }
    public function getCategorias(){
        $datos=null;
        $conexion = $this->getConexion();
        $sql = /** @lang MariaDB */
            "SELECT c.id, c.nombre FROM categorias as c";
        $consulta = $conexion->query($sql);
        $consulta->bindColumn(1, $idCategoria);
        $consulta->bindColumn(2, $nombreCategoria);
        while ($fila = $consulta->fetch(PDO::FETCH_BOUND)) {
            $datos[] = new Categoria($idCategoria, $nombreCategoria);
        }
        return $datos;
    }
}

