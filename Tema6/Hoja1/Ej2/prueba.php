<?php 
require_once( 'Coche.php' );
?>
<!DOCTYPE html>
<html lang='en'>
<head>
    <meta charset='UTF-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1, shrink-to-fit=no'>
    <meta http-equiv='X-UA-Compatible' content='ie=edge'>
    <title>Document</title>
    <link href='https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css' rel='stylesheet'
     integrity='sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6' crossorigin='anonymous'>
</head>
<body class="jumbotron">
<h1>Vehiculos</h1>
    <?php 
    
    $coche1=new Coche("7777J", 110);
    $coche2=new Coche("6666l", 40);
    $coche3=new Coche("33333h", 50);

    echo "Vehiculo 1: $coche1";
    echo "<br>";
    echo "Vehiculo 2: $coche2";
    echo "<br>";
    echo "Vehiculo 1 despues de frenar: ". $coche1->acelerar(60). $coche1;
    echo "<br>";
    echo "Vehiculo 2 despues de acelerar: ". $coche2->frenar(20). $coche2;
    echo "<br>";
    echo "Vehiculo pasado de velocidad: ".$coche3->frenar(120). $coche3;
    
    ?>
</body>
<script src='https://code.jquery.com/jquery-3.2.1.slim.min.js'
    integrity='sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN'
    crossorigin='anonymous'></script>
<script src='https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.1/dist/umd/popper.min.js'
    integrity='sha384-SR1sx49pcuLnqZUnnPwx6FCym0wLsk5JZuNx2bPPENzswTNFaQU1RDvt3wT4gWFG' crossorigin='anonymous'></script>
<script src='https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.min.js'
     integrity='sha384-j0CNLUeiqtyaRmlzUHCPZ+Gy5fQu0dQ6eZ/xAww941Ai1SxSY+0EQqNXNE6DZiVc' crossorigin='anonymous'></script>
</html>