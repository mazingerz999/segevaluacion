<?php  
require_once( 'Cuenta.php' );
require_once( 'CuentaAhorro.php' );
require_once( 'CuentaCorriente.php' );

$cuenta=new Cuenta("Santander", "Jose", 700);
$cuentaAhorro=new CuentaAhorro("Santander", "Alberto", 700,15,20);
$cuentaCorriente=new CuentaCorriente("Bankia", "Ivan", 700,50);

if ($cuenta->esPreferencial(20)) {
    echo "<h3>CUENTAS </h3>";
    echo "<br>";
    echo "$cuenta y es Preferencial";
    echo "<br>";
}
$cuenta->ingreso(50);
echo "Ingreso 50 €";
echo "<br>";
echo "$cuenta";
$cuenta->reintegro(20);
echo "<br>";
echo "Saco 20 €";
echo "<br>";
echo "$cuenta";
echo "<br>";
/////////////////////////////////CuentaFin
echo "<h3>CUENTAS CORRIENTES</h3> <br>";
$cuentaCorriente->ingreso(50);
echo "Ingreso 50 €";
echo "<br>";
echo "$cuentaCorriente";
$cuentaCorriente->reintegro(20);
echo "<br>";
echo "Saco 20 €";
echo "<br>";
echo "$cuentaCorriente";
echo "<br>";
//////////////////////////////CuentaCorrienteFin
echo "<h3>CUENTAS AHORRO</h3> <br>";

echo "$cuentaAhorro";
echo "<br>";
$cuentaAhorro->aplicaInteres();
echo "Aplico interes";
echo "<br>";
echo "$cuentaAhorro";


?>
<!DOCTYPE html>
<html lang='en'>
<head>
    <meta charset='UTF-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1, shrink-to-fit=no'>
    <meta http-equiv='X-UA-Compatible' content='ie=edge'>
    <title>Document</title>
    <link href='https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css' rel='stylesheet'
     integrity='sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6' crossorigin='anonymous'>
</head>
<body>
    
</body>
<script src='https://code.jquery.com/jquery-3.2.1.slim.min.js'
    integrity='sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN'
    crossorigin='anonymous'></script>
<script src='https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.1/dist/umd/popper.min.js'
    integrity='sha384-SR1sx49pcuLnqZUnnPwx6FCym0wLsk5JZuNx2bPPENzswTNFaQU1RDvt3wT4gWFG' crossorigin='anonymous'></script>
<script src='https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.min.js'
     integrity='sha384-j0CNLUeiqtyaRmlzUHCPZ+Gy5fQu0dQ6eZ/xAww941Ai1SxSY+0EQqNXNE6DZiVc' crossorigin='anonymous'></script>
</html>