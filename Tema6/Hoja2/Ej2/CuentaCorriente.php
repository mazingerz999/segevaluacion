<?php 
require_once( 'Cuenta.php' );
class CuentaCorriente extends Cuenta{

private $cuota_mantenimiento;

public function __construct($nombre, $titular, $saldo,$cuota_mantenimiento)
{
    parent::__construct($nombre, $titular, $saldo);
    $this->cuota_mantenimiento = $cuota_mantenimiento;
    if ($cuota_mantenimiento < $this->saldo) {
        $this->saldo -= $cuota_mantenimiento;
    }
}



/**
 * Get the value of cuota_mantenimiento
 */ 
public function getCuota_mantenimiento()
{
return $this->cuota_mantenimiento;
}

/**
 * Set the value of cuota_mantenimiento
 *
 * @return  self
 */ 
public function setCuota_mantenimiento($cuota_mantenimiento)
{
$this->cuota_mantenimiento = $cuota_mantenimiento;

return $this;
}
public function reintegro($cantidad)
{
    if ($this->saldo > 20 && $this->saldo > $cantidad) {
        $this->saldo -= $cantidad;
    }
}
public function __toString()
{
  return parent:: __toString(). " Cuota de mantenimiento: ".$this->getCuota_mantenimiento();
}


}
