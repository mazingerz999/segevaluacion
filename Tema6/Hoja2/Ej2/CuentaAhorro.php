<?php 
require_once( 'Cuenta.php' );

class CuentaAhorro extends Cuenta{

    private $comision_apertura;
    private $interes;


    public function __construct($nombre, $titular, $saldo,$comision_apertura, $interes)
    {
        parent::__construct($nombre, $titular, $saldo);
        $this->comision_apertura=$comision_apertura;
        if ($this->saldo>$comision_apertura) {
            $this->saldo-=$comision_apertura;
        }
        $this->interes=$interes;

    }

    /**
     * Get the value of comision_apertura
     */ 
    public function getComision_apertura()
    {
        return $this->comision_apertura;
    }

    /**
     * Set the value of comision_apertura
     *
     * @return  self
     */ 
    public function setComision_apertura($comision_apertura)
    {
        $this->comision_apertura = $comision_apertura;

        return $this;
    }

    /**
     * Get the value of interes
     */ 
    public function getInteres()
    {
        return $this->interes;
    }

    /**
     * Set the value of interes
     *
     * @return  self
     */ 
    public function setInteres($interes)
    {
        $this->interes = $interes;

        return $this;
    }
    public function aplicaInteres()
    {
     

        return $this->saldo+=$this->getInteres();
    }

    public function __toString()
{
  return parent::__toString()." Comision de apertura".$this->getComision_apertura()." Interes: ". $this->getInteres();
}
}
