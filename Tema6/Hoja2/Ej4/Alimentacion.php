<?php
require_once ('Productos.php');

class Alimentacion extends Productos
{
private $mesCaducidad;
    private $anyoCaducidad;

    /**
     * Alimentacion constructor.
     * @param $mesCaducidad
     * @param $anyoCaducidad
     */
    public function __construct($codigo, $precio, $nombre,$mesCaducidad, $anyoCaducidad)
    {
        parent::__construct($codigo, $precio, $nombre);
        $this->mesCaducidad = $mesCaducidad;
        $this->anyoCaducidad = $anyoCaducidad;
    }

    /**
     * @return mixed
     */
    public function getMesCaducidad()
    {
        return $this->mesCaducidad;
    }

    /**
     * @param mixed $mesCaducidad
     */
    public function setMesCaducidad($mesCaducidad): void
    {
        $this->mesCaducidad = $mesCaducidad;
    }

    /**
     * @return mixed
     */
    public function getAnyoCaducidad()
    {
        return $this->anyoCaducidad;
    }

    /**
     * @param mixed $anyoCaducidad
     */
    public function setAnyoCaducidad($anyoCaducidad): void
    {
        $this->anyoCaducidad = $anyoCaducidad;
    }

    public function __toString()
    {
        return parent::__toString(). " Mes caducidad: ".$this->getMesCaducidad(). " Año de caducidad: ".$this->getAnyoCaducidad();
    }

}