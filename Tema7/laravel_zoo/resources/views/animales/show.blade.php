@extends('layouts.master')

@section('titulo')
    Zoologico
@endsection

@section('contenido')
<div class="row">
    <div class="col-sm-3">
        <img src="{{asset('assets/imagenes')}}/{{$animal->imagen}}" style="height:200px"/>
    </div>
    <div class="col-sm-9">
        <p class="h3">{{$animal->especie}}</p>
        <p class="h5">Peso</p>
        <p>{{$animal->peso}} Kg</p>
        <p class="h3">Altura</p>
        <p>{{$animal->altura}} cm</p>
        <p class="h5">Descripcion</p>
        <p>{{$animal->descripcion}}</p>
        <p class="h6">Revisiones</p>
       @foreach ($animal->revisiones as $revision)
           <ul>
               <li>Descripcion: {{$revision->descripcion}}</li>
               <li>Fecha: {{$revision->fecha}}</li>
           </ul>
        @endforeach

        <div>
           <a href="{{route('animales.edit' , $animal )}}"><button type="button" class="btn btn-warning">Editado</button></a>
           <a href="{{route('revisiones.create' , $animal )}}"><button type="button" class="btn btn-warning">Add Revision</button></a>
            <a href="{{route('animales.index')}}"><button type="button" class="btn btn-secondary">Volver al Listado</button></a>

    </div>
    </div>
@endsection
