<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cuidador extends Model
{
    use HasFactory;
    protected $table="cuidadores";
    protected $guarded=[];

    public function animales()
    {
        # code...
        return $this->belongsToMany(Cuidador::class);
    }
    public function titulaciones()
    {
        # code...
        return $this->belongsTo(Titulacion::class, "id_titulacion1")->get()
        ->merge($this->belongsTo(Titulacion::class, "id_titulacion2")->get());
    }
    public function getRouteKeyName()
    {
        # code...
        return "slug";
    }

}
