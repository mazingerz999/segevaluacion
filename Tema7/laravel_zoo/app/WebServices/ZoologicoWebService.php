<?php
namespace App\WebServices;
use App\Models\Animal;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class ZoologicoWebService {

/**
 * Funcion para sacar login
 *
 * @param string $user
 * @param string $password
 * @return boolean
 */
public function login($email, $password){
    # code...

    return Auth::attempt(['email' => $email, 'password' => $password]);

}
/**
 * Funcion para traer un animal en concreto
 *
 * @param string $id
 * @return App\Models\Animal
 */
public function getAnimal($id){
    # code...
    $animal=Animal::find($id);

    return $animal;

}
/**
 * Traer todos los animales de la BD
 *
 * @return App\Models\Animal[]
 */
public function getAnimales(){
    # code...
    $all=[];
    $all=Animal::all();
    return $all;
}
/**
 * Filtrado de animales por alimentacion
 *
 * @param string $alimentacion
 * @return App\Models\Animal[]
 */
public function getAnimalesAlimentacion($alimentacion){
    # code...
    $comidas=[];
    $comidas=Animal::where('alimentacion', $alimentacion)->get();

    return $comidas;

}
/**
 * Funcion buscador por especie
 *
 * @param string $especie
 * @return App\Models\Animal[]
 */
public function busqueda($especie){
    # code...
    $search=[];
    $search=Animal::where('especie', 'like', '%'.$especie.'%')->get();


    return $search;
}

}
